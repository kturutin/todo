Provide spring config file using argument:
  -Dspring.config.location=file:<path_to_properties_config>
  
See auth.external.properties for example.