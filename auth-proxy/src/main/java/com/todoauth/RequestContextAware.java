package com.todoauth;

import com.netflix.zuul.context.RequestContext;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

public interface RequestContextAware {

    default RequestContext getZuulContextRequired() {
        return getZuulContext().orElseThrow(() -> new IllegalStateException("No Zuul request context available!"));
    }

    default SecurityContext getSecurityContextRequired() {
        return getSecurityContext().orElseThrow(() -> new IllegalStateException("No security request context available!"));
    }

    default Optional<RequestContext> getZuulContext() {
        return Optional.ofNullable(RequestContext.getCurrentContext());
    }

    default Optional<SecurityContext> getSecurityContext() {
        return Optional.ofNullable(SecurityContextHolder.getContext());
    }
}
