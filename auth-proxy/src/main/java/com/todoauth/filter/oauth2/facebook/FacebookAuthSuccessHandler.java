package com.todoauth.filter.oauth2.facebook;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.todoauth.authentication.facebook.FacebookProfile;
import com.todoauth.filter.oauth2.AbstractAuthSuccessHandler;
import com.todoauth.session.UserSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Component;

@Component
public class FacebookAuthSuccessHandler extends AbstractAuthSuccessHandler<FacebookProfile> {

    @Autowired
    public FacebookAuthSuccessHandler(UserSession session, OAuth2ClientContext oAuthContext,
                                      @Qualifier("prettyPrintObjectMapper") ObjectMapper objectMapper) {
        super(session, oAuthContext, objectMapper);
    }

    @Override
    public FacebookProfile mapAuthToProfile(OAuth2Authentication authResult) {
        return objectMapper.convertValue(authResult.getUserAuthentication().getDetails(), FacebookProfile.class);
    }
}
