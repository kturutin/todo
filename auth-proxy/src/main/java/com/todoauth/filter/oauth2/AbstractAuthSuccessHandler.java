package com.todoauth.filter.oauth2;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.todoauth.RequestContextAware;
import com.todoauth.authentication.OAuthAuthentication;
import com.todoauth.authentication.BasicProfile;
import com.todoauth.session.UserSession;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class AbstractAuthSuccessHandler<T extends BasicProfile> implements AuthenticationSuccessHandler, RequestContextAware {

    protected final ObjectMapper objectMapper;
    private final UserSession session;
    private final OAuth2ClientContext oAuthContext;

    @Value("${server.session.timeoutInSeconds}")
    private int sessionTimeoutInSeconds;

    public AbstractAuthSuccessHandler(UserSession session, OAuth2ClientContext oAuthContext, ObjectMapper objectMapper) {
        this.session = session;
        this.oAuthContext = oAuthContext;
        this.objectMapper = objectMapper;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        if (!(authentication instanceof OAuth2Authentication)) {
            throw new IllegalStateException("Unknown authentication result type: " + authentication.getClass());
        }
        OAuth2Authentication auth = (OAuth2Authentication) authentication;
        OAuth2AccessToken accessToken = oAuthContext.getAccessToken();

        session.setAccessToken(accessToken.getValue());
        session.setRefreshToken(accessToken.getRefreshToken() != null ? accessToken.getRefreshToken().getValue() : null);
        session.setProfile(mapAuthToProfile(auth));

        request.getSession().setMaxInactiveInterval(sessionTimeoutInSeconds);
        overrideAuthenticationInContext(auth);
    }

    /**
     * Storing com.todoauth.authentication.OAuthAuthentication in context instead of org.springframework.security.oauth2.provider.OAuth2Authentication
     * @see OAuthAuthentication for comments
     * @param auth Spring's auth object
     */
    private void overrideAuthenticationInContext(OAuth2Authentication auth) {
        Authentication newAuth = new OAuthAuthentication(auth.getOAuth2Request(), auth.getUserAuthentication());
        getSecurityContextRequired().setAuthentication(newAuth);
    }

    public abstract T mapAuthToProfile(OAuth2Authentication authResult);
}
