package com.todoauth.filter.oauth2.facebook;

import com.todoauth.authentication.facebook.FacebookOAuth2RestTemplate;
import com.todoauth.authentication.facebook.FacebookUserInfoTokenServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.client.filter.OAuth2ClientAuthenticationProcessingFilter;
import org.springframework.stereotype.Component;

@Component
public class FacebookOAuth2ClientAuthProcessingFilter extends OAuth2ClientAuthenticationProcessingFilter {

    @Autowired
    public FacebookOAuth2ClientAuthProcessingFilter(FacebookAuthSuccessHandler successHandler,
                                                    FacebookOAuth2RestTemplate restTemplate,
                                                    FacebookUserInfoTokenServices tokenServices,
                                                    @Value("${login.urls.facebook}") String loginUrl) {
        super(loginUrl);
        setRestTemplate(restTemplate);
        setTokenServices(tokenServices);
        setAuthenticationSuccessHandler(successHandler);
    }
}
