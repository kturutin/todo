package com.todoauth.filter.oauth2.google;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.todoauth.authentication.google.GoogleProfile;
import com.todoauth.filter.oauth2.AbstractAuthSuccessHandler;
import com.todoauth.session.UserSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Component;

@Component
public class GoogleAuthSuccessHandler extends AbstractAuthSuccessHandler<GoogleProfile> {

    @Autowired
    public GoogleAuthSuccessHandler(UserSession session, OAuth2ClientContext oAuthContext,
                                    @Qualifier("prettyPrintObjectMapper") ObjectMapper objectMapper) {
        super(session, oAuthContext, objectMapper);
    }

    @Override
    public GoogleProfile mapAuthToProfile(OAuth2Authentication authResult) {
        return objectMapper.convertValue(authResult.getUserAuthentication().getDetails(), GoogleProfile.class);
    }
}
