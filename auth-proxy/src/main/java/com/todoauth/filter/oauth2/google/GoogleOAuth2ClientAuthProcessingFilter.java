package com.todoauth.filter.oauth2.google;

import com.todoauth.authentication.google.GoogleOAuth2RestTemplate;
import com.todoauth.authentication.google.GoogleUserInfoTokenServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.client.filter.OAuth2ClientAuthenticationProcessingFilter;
import org.springframework.stereotype.Component;

@Component
public class GoogleOAuth2ClientAuthProcessingFilter extends OAuth2ClientAuthenticationProcessingFilter {

    @Autowired
    public GoogleOAuth2ClientAuthProcessingFilter(GoogleAuthSuccessHandler successHandler,
                                                  GoogleOAuth2RestTemplate restTemplate,
                                                  GoogleUserInfoTokenServices tokenServices,
                                                  @Value("${login.urls.google}") String loginUrl) {
        super(loginUrl);
        setRestTemplate(restTemplate);
        setTokenServices(tokenServices);
        setAuthenticationSuccessHandler(successHandler);
    }
}
