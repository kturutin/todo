package com.todoauth.filter.oauth2;

import com.todoauth.RequestContextAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class LoginFilter implements Filter, RequestContextAware {

    private final RequestMatcher loginRequestMatcher;

    @Value("${login.successfulAuthenticatedRedirectUrl}")
    private String successfulAuthenticatedRedirectUrl;

    @Value("${server.session.sessionUrlParamName}")
    private String sessionUrlParamName;

    @Autowired
    public LoginFilter(@Value("${login.urlPattern}") String loginUrlPattern) {
        this.loginRequestMatcher = new AntPathRequestMatcher(loginUrlPattern);
    }

    /**
     * Disable auto-registration
     */
    @Bean
    public FilterRegistrationBean loginFilterRegistration(LoginFilter filter) {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(filter);
        filterRegistrationBean.setEnabled(false);
        return filterRegistrationBean;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        if (!isProcessingRequired((HttpServletRequest) req)) {
            chain.doFilter(req, res);
            return;
        }

        HttpServletResponse response = ((HttpServletResponse) res);

        if (isAuthenticated()) {
            onSuccessfulAuthentication(response);
        } else {
            chain.doFilter(req, res);
            if (isAuthenticated()) {
                onSuccessfulAuthentication(response);
            }
        }
    }

    private void onSuccessfulAuthentication(HttpServletResponse res) {
        if (!res.isCommitted()) {
            try {
                res.sendRedirect(successfulAuthenticatedRedirectUrl);
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    private boolean isAuthenticated() {
        return getSecurityContextRequired().getAuthentication() != null;
    }

    private boolean isProcessingRequired(HttpServletRequest request) {
        return loginRequestMatcher.matches(request);
    }

    @Override
    public void destroy() {
    }
}
