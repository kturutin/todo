package com.todoauth.filter.oauth2;

import com.todoauth.filter.oauth2.facebook.FacebookOAuth2ClientAuthProcessingFilter;
import com.todoauth.filter.oauth2.google.GoogleOAuth2ClientAuthProcessingFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.CompositeFilter;

import javax.annotation.PostConstruct;
import javax.servlet.Filter;
import java.util.ArrayList;
import java.util.List;

@Component
public class OAuth2ClientAuthProcessingCompositeFilter extends CompositeFilter {

    @Autowired
    public OAuth2ClientAuthProcessingCompositeFilter(FacebookOAuth2ClientAuthProcessingFilter facebookOAuth2ClientAuthProcessingFilter,
                                                     GoogleOAuth2ClientAuthProcessingFilter googleOAuth2ClientAuthProcessingFilter) {
        List<Filter> filters = new ArrayList<>(2);
        filters.add(facebookOAuth2ClientAuthProcessingFilter);
        filters.add(googleOAuth2ClientAuthProcessingFilter);
        setFilters(filters);
    }

    /**
     * Disable auto-registration
     */
    @Bean
    private FilterRegistrationBean oAuth2ClientAuthProcessingCompositeFilterRegistration(OAuth2ClientAuthProcessingCompositeFilter filter) {
        FilterRegistrationBean reg = new FilterRegistrationBean(filter);
        reg.setEnabled(false);
        return reg;
    }
}
