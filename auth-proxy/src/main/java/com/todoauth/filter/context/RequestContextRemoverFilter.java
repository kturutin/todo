package com.todoauth.filter.context;

import org.springframework.boot.web.filter.OrderedRequestContextFilter;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.session.web.http.SessionRepositoryFilter;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class RequestContextRemoverFilter extends OrderedRequestContextFilter {
    private static final int BEFORE_SESSION_REPOSITORY_FILTER = SessionRepositoryFilter.DEFAULT_ORDER - 1;

    @Override
    public int getOrder() {
        return BEFORE_SESSION_REPOSITORY_FILTER;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        resetContextHolders();
        if (logger.isDebugEnabled()) {
            logger.debug("Cleared thread-bound request context: " + request);
        }
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes instanceof ServletRequestAttributes) {
            ((ServletRequestAttributes) requestAttributes).requestCompleted();
        }

        filterChain.doFilter(request, response);
    }

    private void resetContextHolders() {
        LocaleContextHolder.resetLocaleContext();
        RequestContextHolder.resetRequestAttributes();
    }
}
