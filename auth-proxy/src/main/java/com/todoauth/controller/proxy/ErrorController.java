package com.todoauth.controller.proxy;


import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;
import java.util.UUID;

@Controller
@RequestMapping("${server.error.location}")
public class ErrorController extends AbstractErrorController {
    public static final String ERROR_ATTRIBUTES_REQUEST_ATTRIBUTE = ErrorController.class.getSimpleName() + ".error.details";

    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorController.class);

    @Value("${server.error.location}")
    private String errorPath;

    private final DefaultErrorAttributes errorAttributes;

    @Autowired
    public ErrorController(DefaultErrorAttributes errorAttributes) {
        super(errorAttributes);
        this.errorAttributes = errorAttributes;
    }

    @Override
    public String getErrorPath() {
        return errorPath;
    }

    @RequestMapping
    @ResponseBody
    public ErrorMessage handleError(HttpServletRequest request) {
        return getErrorMessage(request, false);
    }

    private ErrorMessage getErrorMessage(HttpServletRequest request, boolean includeStackTrace) {
        return getErrorAttributesFromRequestAttrs(request).orElse(createErrorAttributes(request, includeStackTrace));
    }

    private ErrorMessage createErrorAttributes(HttpServletRequest request, boolean includeStackTrace) {
        ErrorMessage errorMessage = new ErrorMessage(super.getErrorAttributes(request, includeStackTrace));
        boolean isInternalError = getStatus(request).is5xxServerError();
        errorMessage = isInternalError ? hideErrorDetails(request, errorMessage) : errorMessage;
        request.setAttribute(ERROR_ATTRIBUTES_REQUEST_ATTRIBUTE, errorMessage);
        return errorMessage;
    }

    private Optional<ErrorMessage> getErrorAttributesFromRequestAttrs(HttpServletRequest request) {
        return Optional.ofNullable((ErrorMessage) request.getAttribute(ERROR_ATTRIBUTES_REQUEST_ATTRIBUTE));
    }

    private ErrorMessage hideErrorDetails(HttpServletRequest request, ErrorMessage result) {
        result.remove(ErrorMessage.Field.TRACE);
        result.remove(ErrorMessage.Field.EXCEPTION);
        result.put(ErrorMessage.Field.MESSAGE, getHiddenErrorMessage(request));
        return result;
    }

    private String getHiddenErrorMessage(HttpServletRequest request) {
        Throwable throwable = errorAttributes.getError(new ServletRequestAttributes(request));
        if (throwable != null) {
            String meditationId = UUID.randomUUID().toString();
            LOGGER.warn("Error " + meditationId, throwable);
            return "Meditation id: " + meditationId;
        }
        return StringUtils.EMPTY;
    }
}
