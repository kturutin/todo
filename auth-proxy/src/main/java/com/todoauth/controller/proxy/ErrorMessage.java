package com.todoauth.controller.proxy;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ErrorMessage extends HashMap<ErrorMessage.Field, Object> {

    public ErrorMessage(Map<String, Object> data) {
        data.forEach((key, value) -> {
            Field.getEnum(key).ifPresent(enumValue -> put(enumValue, value));
        });
    }

    public enum Field {
        TIMESTAMP,
        STATUS,
        ERROR,
        EXCEPTION,
        MESSAGE,
        TRACE;

        private static Optional<Field> getEnum(String value) {
            return Arrays.stream(values()).filter(f -> f.name().equals(value.toUpperCase())).findFirst();
        }
    }
}
