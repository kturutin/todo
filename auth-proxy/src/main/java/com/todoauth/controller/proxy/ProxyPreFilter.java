package com.todoauth.controller.proxy;

import com.netflix.zuul.ZuulFilter;
import com.todoauth.RequestContextAware;
import com.todoauth.authentication.BasicProfile;
import com.todoauth.session.UserSession;
import com.todocommon.authentication.jwt.Claim;
import com.todocommon.authentication.jwt.TokenCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class ProxyPreFilter extends ZuulFilter implements RequestContextAware {

    private final String tokenName;
    private final String tokenSecret;
    private final UserSession session;

    @Autowired
    public ProxyPreFilter(UserSession session,
                          @Value("${target.jwt.token-name}") String tokenName,
                          @Value("${target.jwt.secret}") String tokenSecret) {
        this.session = session;
        this.tokenName = tokenName;
        this.tokenSecret = tokenSecret;
    }

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        getZuulContextRequired().addZuulRequestHeader(tokenName, createToken());
        return null;
    }

    private String createToken() {
        return TokenCreator.createToken(createClaims(), tokenSecret);
    }

    private Map<Claim, String> createClaims() {
        BasicProfile profile = session.getProfile();
        Map<Claim, String> result = new HashMap<>();

        result.put(Claim.ID, profile.getId());
        result.put(Claim.SUBJECT, profile.getName());
        result.put(Claim.GIVEN_NAME, profile.getGivenName());
        result.put(Claim.FAMILY_NAME, profile.getFamilyName());
        result.put(Claim.EMAIL, profile.getEmail());
        result.put(Claim.ROLE, getAuthoritiesValue());

        return result;
    }

    private String getAuthoritiesValue() {
        return getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.joining(","));
    }

    private Collection<? extends GrantedAuthority> getAuthorities() {
        Authentication authentication = getSecurityContextRequired().getAuthentication();

        if (authentication == null) {
            throw new IllegalStateException("No Authentication available in the context, stop proxying.");
        }

        return Optional.
                ofNullable(authentication.getAuthorities()).
                orElseThrow(() -> new IllegalStateException("No authorities available in the token, stop proxying."));
    }
}
