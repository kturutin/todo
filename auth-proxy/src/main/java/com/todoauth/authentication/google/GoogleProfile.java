package com.todoauth.authentication.google;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.todoauth.authentication.BasicProfile;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GoogleProfile implements BasicProfile, Serializable {

    @JsonProperty(value = "sub", required = true)
    private String id;

    private String name;

    @JsonProperty("given_name")
    private String givenName;

    @JsonProperty("family_name")
    private String familyName;

    @JsonProperty("profile")
    private String profileUrl;

    @JsonProperty("picture")
    private String pictureUrl;

    private String locale;

    private String email;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getGivenName() {
        return givenName;
    }

    @Override
    public String getFamilyName() {
        return familyName;
    }

    @Override
    public String getProfileUrl() {
        return profileUrl;
    }

    @Override
    public String getPictureUrl() {
        return pictureUrl;
    }

    @Override
    public String getLocale() {
        return locale;
    }

    @Override
    public String getEmail() {
        return email;
    }
}
