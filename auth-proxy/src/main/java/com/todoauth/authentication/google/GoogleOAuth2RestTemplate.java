package com.todoauth.authentication.google;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.stereotype.Component;

@Component
public class GoogleOAuth2RestTemplate extends OAuth2RestTemplate {

    public GoogleOAuth2RestTemplate(@Qualifier("google") AuthorizationCodeResourceDetails clientRegistrationDetails,
                                    OAuth2ClientContext oAuth2ClientContext) {
        super(clientRegistrationDetails, oAuth2ClientContext);
    }
}
