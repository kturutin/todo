package com.todoauth.authentication.google;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;
import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Primary
public class GoogleUserInfoTokenServices extends UserInfoTokenServices {

    private final GoogleOAuth2RestTemplate oAuth2RestTemplate;

    @Autowired
    public GoogleUserInfoTokenServices(@Qualifier("google") ResourceServerProperties resourceProperties,
                                       @Qualifier("google") AuthorizationCodeResourceDetails clientDetails, GoogleOAuth2RestTemplate oAuth2RestTemplate) {
        super(resourceProperties.getUserInfoUri(), clientDetails.getClientId());
        this.oAuth2RestTemplate = oAuth2RestTemplate;
    }

    @PostConstruct
    public void init() {
        setRestTemplate(oAuth2RestTemplate);
    }
}
