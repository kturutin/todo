package com.todoauth.authentication;

public interface BasicProfile {
    String getId();

    String getName();

    String getGivenName();

    String getFamilyName();

    String getProfileUrl();

    String getPictureUrl();

    String getLocale();

    String getEmail();
}
