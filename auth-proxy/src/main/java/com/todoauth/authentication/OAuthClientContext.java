package com.todoauth.authentication;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.token.AccessTokenRequest;
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

import java.util.HashMap;
import java.util.Map;

/**
 * Major reason to create this class (which is in fact copy/paste of DefaultOAuth2ClientContext)
 * is making it easier to serialize/deserialize OAuth2ClientContext
 */
public class OAuthClientContext extends DefaultOAuth2ClientContext {

    private OAuth2AccessToken accessToken;
    private Map<String, Object> state = new HashMap<>();

    @JsonIgnore
    private AccessTokenRequest accessTokenRequest;

    public OAuthClientContext() {
        this(new DefaultAccessTokenRequest());
    }

    public OAuthClientContext(AccessTokenRequest accessTokenRequest) {
        this.accessTokenRequest = accessTokenRequest;
    }

    public OAuthClientContext(OAuth2AccessToken accessToken) {
        this.accessToken = accessToken;
        this.accessTokenRequest = new DefaultAccessTokenRequest();
    }

    @Override
    public OAuth2AccessToken getAccessToken() {
        return accessToken;
    }

    @Override
    public void setAccessToken(OAuth2AccessToken accessToken) {
        this.accessToken = accessToken;
        this.accessTokenRequest.setExistingToken(accessToken);
    }

    @Override
    public AccessTokenRequest getAccessTokenRequest() {
        return accessTokenRequest;
    }

    public void setAccessTokenRequest(AccessTokenRequest accessTokenRequest) {
        this.accessTokenRequest = accessTokenRequest;
    }

    @Override
    public void setPreservedState(String stateKey, Object preservedState) {
        state.put(stateKey, preservedState);
    }

    @Override
    public Object removePreservedState(String stateKey) {
        return state.remove(stateKey);
    }

    public Map<String, Object> getState() {
        return state;
    }

    public void setState(Map<String, Object> state) {
        this.state = state;
    }
}
