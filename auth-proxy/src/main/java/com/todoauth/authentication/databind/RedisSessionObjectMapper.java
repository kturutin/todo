package com.todoauth.authentication.databind;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.jackson2.CoreJackson2Module;
import org.springframework.security.web.jackson2.WebJackson2Module;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Qualifier("redisSessionObjectMapper")
public class RedisSessionObjectMapper extends ObjectMapper {
    private static final String TYPING_PROPERTY_NAME = "@class";

    @Value("${security.oauth2.session.usePrettyPrint:false}")
    private boolean usePrettyPrint;

    private final OAuthClientContextJacksonModule oAuth2ClientContextModule;

    @Autowired
    public RedisSessionObjectMapper(OAuthClientContextJacksonModule oAuth2ClientContextModule) {
        this.oAuth2ClientContextModule = oAuth2ClientContextModule;
    }

    @PostConstruct
    private void init() {
        if (usePrettyPrint) {
            enable(SerializationFeature.INDENT_OUTPUT);
        }
        enableDefaultTypingAsProperty(ObjectMapper.DefaultTyping.NON_FINAL, TYPING_PROPERTY_NAME);
        registerModule(oAuth2ClientContextModule);
        registerModule(new CoreJackson2Module());
        registerModule(new WebJackson2Module());
    }
}
