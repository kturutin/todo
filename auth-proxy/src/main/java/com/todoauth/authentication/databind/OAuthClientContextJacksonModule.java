package com.todoauth.authentication.databind;

import com.fasterxml.jackson.databind.module.SimpleModule;
import com.todoauth.authentication.OAuthClientContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class OAuthClientContextJacksonModule extends SimpleModule {

    private final OAuthContextDeserializer OAuthContextDeserializer;

    @Autowired
    public OAuthClientContextJacksonModule(OAuthContextDeserializer OAuthContextDeserializer) {
        this.OAuthContextDeserializer = OAuthContextDeserializer;
    }

    @PostConstruct
    private void init() {
        addDeserializer(OAuthClientContext.class, OAuthContextDeserializer);
    }
}
