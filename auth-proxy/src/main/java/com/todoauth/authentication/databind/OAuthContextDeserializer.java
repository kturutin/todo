package com.todoauth.authentication.databind;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.todoauth.authentication.OAuthClientContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.token.AccessTokenRequest;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * AccessTokenRequest is request-scoped, so need to inject and set it manually
 */
@Component
public class OAuthContextDeserializer extends JsonDeserializer<OAuthClientContext> {

    private final AccessTokenRequest accessTokenRequest;
    private final ObjectMapper objectMapper;

    @Autowired
    public OAuthContextDeserializer(AccessTokenRequest accessTokenRequest, ObjectMapper objectMapper) {
        this.accessTokenRequest = accessTokenRequest;
        this.objectMapper = objectMapper;
    }

    @Override
    public OAuthClientContext deserialize(JsonParser parser, DeserializationContext ctxt) throws IOException {
        OAuthClientContext context = objectMapper.readValue(parser, OAuthClientContext.class);
        context.setAccessTokenRequest(accessTokenRequest);
        return context;
    }
}
