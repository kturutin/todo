package com.todoauth.authentication.facebook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class FacebookUserInfoTokenServices extends UserInfoTokenServices {

    private final FacebookOAuth2RestTemplate oAuth2RestTemplate;

    @Autowired
    public FacebookUserInfoTokenServices(@Qualifier("facebook") ResourceServerProperties resourceProperties,
                                         @Qualifier("facebook") AuthorizationCodeResourceDetails clientDetails, FacebookOAuth2RestTemplate oAuth2RestTemplate) {
        super(resourceProperties.getUserInfoUri(), clientDetails.getClientId());
        this.oAuth2RestTemplate = oAuth2RestTemplate;
    }

    @PostConstruct
    public void init() {
        setRestTemplate(oAuth2RestTemplate);
    }
}
