package com.todoauth.authentication.facebook;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.todoauth.authentication.BasicProfile;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FacebookProfile implements BasicProfile, Serializable {

    @JsonProperty(value = "id", required = true)
    private String id;

    private String name;

    @JsonProperty("first_name")
    private String givenName;

    @JsonProperty("last_name")
    private String familyName;

    @JsonProperty("link")
    private String profileUrl;

    @JsonProperty("picture")
    @JsonDeserialize(using = FacebookPictureUrlDeserializer.class)
    private String pictureUrl;

    private String locale;

    private String email;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getGivenName() {
        return givenName;
    }

    @Override
    public String getFamilyName() {
        return familyName;
    }

    @Override
    public String getProfileUrl() {
        return profileUrl;
    }

    @Override
    public String getPictureUrl() {
        return pictureUrl;
    }

    @Override
    public String getLocale() {
        return locale;
    }

    @Override
    public String getEmail() {
        return email;
    }
}
