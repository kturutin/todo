package com.todoauth.authentication.facebook;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.stereotype.Component;

@Component
public class FacebookOAuth2RestTemplate extends OAuth2RestTemplate {

    public FacebookOAuth2RestTemplate(@Qualifier("facebook") AuthorizationCodeResourceDetails clientRegistrationDetails,
                                      OAuth2ClientContext oAuth2ClientContext) {
        super(clientRegistrationDetails, oAuth2ClientContext);
    }
}
