package com.todoauth.authentication;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;

/**
 * Slightly changed org.springframework.security.oauth2.provider.OAuth2Authentication to simply serialization/deserialization
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class OAuthAuthentication extends OAuth2Authentication {

    private static final long serialVersionUID = -4809832298438307309L;

    private OAuth2Request storedRequest;

    private Authentication userAuthentication;

    public OAuthAuthentication() {
        super(null, new UsernamePasswordAuthenticationToken(null, null));
    }

    public OAuthAuthentication(OAuth2Request storedRequest, Authentication userAuthentication) {
        super(storedRequest, userAuthentication);
        this.storedRequest = storedRequest;
        this.userAuthentication = userAuthentication;
    }

    public Object getCredentials() {
        return "";
    }

    public Object getPrincipal() {
        return this.userAuthentication == null ? this.storedRequest.getClientId() : this.userAuthentication
                .getPrincipal();
    }

    /**
     * Convenience method to check if there is a user associated with this token, or just a client application.
     *
     * @return true if this token represents a client app not acting on behalf of a user
     */
    public boolean isClientOnly() {
        return userAuthentication == null;
    }

    /**
     * The authorization request containing details of the client application.
     *
     * @return The client authentication.
     */
    public OAuth2Request getOAuth2Request() {
        return storedRequest;
    }

    /**
     * The user authentication.
     *
     * @return The user authentication.
     */
    public Authentication getUserAuthentication() {
        return userAuthentication;
    }

    @Override
    public boolean isAuthenticated() {
        return storedRequest != null && this.storedRequest.isApproved()
                && (this.userAuthentication == null || this.userAuthentication.isAuthenticated());
    }

    @Override
    public void eraseCredentials() {
        super.eraseCredentials();
        if (this.userAuthentication != null && CredentialsContainer.class.isAssignableFrom(this.userAuthentication.getClass())) {
            CredentialsContainer.class.cast(this.userAuthentication).eraseCredentials();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OAuthAuthentication)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        OAuthAuthentication that = (OAuthAuthentication) o;

        if (!storedRequest.equals(that.storedRequest)) {
            return false;
        }
        if (userAuthentication != null ? !userAuthentication.equals(that.userAuthentication)
                : that.userAuthentication != null) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "OAuthAuthentication{" +
                "storedRequest=" + storedRequest +
                ", userAuthentication=" + userAuthentication +
                '}';
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + storedRequest.hashCode();
        result = 31 * result + (userAuthentication != null ? userAuthentication.hashCode() : 0);
        return result;
    }
}
