package com.todoauth.authentication;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.token.AccessTokenRequest;
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest;

import java.io.IOException;

public class OAuth2ClientContextSerializer extends StdSerializer<DefaultOAuth2ClientContext> {

    public OAuth2ClientContextSerializer() {
        super(DefaultOAuth2ClientContext.class);
    }

    @Override
    public void serialize(DefaultOAuth2ClientContext value, JsonGenerator gen, SerializerProvider provider) throws IOException {

    }

    @Override
    public void serializeWithType(DefaultOAuth2ClientContext value, JsonGenerator gen, SerializerProvider serializers, TypeSerializer typeSer) throws IOException {
        AccessTokenRequest accessTokenRequest = value.getAccessTokenRequest();
        if (accessTokenRequest != null) {
            DefaultAccessTokenRequest defaultAccessTokenRequest = new DefaultAccessTokenRequest();
            defaultAccessTokenRequest.putAll(accessTokenRequest);
            typeSer.writeTypePrefixForObject(defaultAccessTokenRequest, gen);
            typeSer.writeTypeSuffixForObject(defaultAccessTokenRequest, gen);
        }
    }
}
