package com.todoauth.config;

import com.todoauth.authentication.RestAuthenticationEntryPoint;
import com.todoauth.filter.oauth2.LoginFilter;
import com.todoauth.filter.oauth2.OAuth2ClientAuthProcessingCompositeFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.filter.OAuth2ClientContextFilter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.web.access.ExceptionTranslationFilter;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.session.SessionManagementFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.servlet.http.HttpServletRequest;

@Component
@EnableWebSecurity
@EnableOAuth2Client
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final OAuth2ClientAuthProcessingCompositeFilter oAuth2ClientAuthProcessingCompositeFilter;
    private final OAuth2ClientContextFilter oAuth2ClientContextFilter;
    private final RestAuthenticationEntryPoint restAuthenticationEntryPoint;
    private final AuthenticationProvider authenticationProvider;
    private final LoginFilter loginFilter;

    @Value("${logout.url}")
    private String logoutUrl;

    @Value("${fe.hostUrl}")
    private String feHostUrl;

    @Autowired
    public SecurityConfig(OAuth2ClientAuthProcessingCompositeFilter oAuth2ClientAuthProcessingCompositeFilter,
                          OAuth2ClientContextFilter oAuth2ClientContextFilter,
                          RestAuthenticationEntryPoint restAuthenticationEntryPoint,
                          AuthenticationProvider authenticationProvider,
                          LoginFilter loginFilter) {
        this.oAuth2ClientAuthProcessingCompositeFilter = oAuth2ClientAuthProcessingCompositeFilter;
        this.oAuth2ClientContextFilter = oAuth2ClientContextFilter;
        this.restAuthenticationEntryPoint = restAuthenticationEntryPoint;
        this.authenticationProvider = authenticationProvider;
        this.loginFilter = loginFilter;
    }

    @Bean
    public FilterRegistrationBean oAuth2ClientContextFilterRegistration() {
        FilterRegistrationBean reg = new FilterRegistrationBean(oAuth2ClientContextFilter);
        reg.setEnabled(false);
        reg.setOrder(-100);
        return reg;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.
                exceptionHandling().
                authenticationEntryPoint(restAuthenticationEntryPoint);

        http.authenticationProvider(authenticationProvider);

        http.cors();

        http.
                antMatcher("/**").
                anonymous().
                disable();

        http.
                logout().
                logoutUrl(logoutUrl).
                clearAuthentication(true).
                invalidateHttpSession(true).
                logoutSuccessUrl("/").
                logoutRequestMatcher(new AntPathRequestMatcher(logoutUrl));

        http.
                addFilterAfter(loginFilter, SessionManagementFilter.class).
                addFilterAfter(oAuth2ClientContextFilter, ExceptionTranslationFilter.class).
                addFilterBefore(oAuth2ClientAuthProcessingCompositeFilter, FilterSecurityInterceptor.class);

        super.configure(http);
    }

    @Bean
    public WebMvcConfigurerAdapter corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.
                        addMapping("/**").
                        allowedOrigins(feHostUrl).
                        allowCredentials(true);
            }
        };
    }
}
