package com.todoauth.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.stereotype.Component;

@Component
public class OAuthGoogleConfigsProvider {

    @Bean
    @Qualifier("google")
    @ConfigurationProperties("security.oauth2.google.client")
    public AuthorizationCodeResourceDetails googleClientRegistrationDetails() {
        return new AuthorizationCodeResourceDetails();
    }

    @Bean
    @Qualifier("google")
    @ConfigurationProperties("security.oauth2.google.resource")
    public ResourceServerProperties googleResourceProperties() {
        return new ResourceServerProperties();
    }
}
