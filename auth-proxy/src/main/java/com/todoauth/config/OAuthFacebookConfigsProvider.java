package com.todoauth.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.stereotype.Component;

@Component
public class OAuthFacebookConfigsProvider {

    @Bean
    @Qualifier("facebook")
    @ConfigurationProperties("security.oauth2.facebook.client")
    public AuthorizationCodeResourceDetails facebookClientRegistrationDetails() {
        return new AuthorizationCodeResourceDetails();
    }

    @Bean
    @Qualifier("facebook")
    @ConfigurationProperties("security.oauth2.facebook.resource")
    public ResourceServerProperties facebookResourceProperties() {
        return new ResourceServerProperties();
    }
}
