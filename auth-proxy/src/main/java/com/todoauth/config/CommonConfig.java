package com.todoauth.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.todoauth.authentication.databind.RedisSessionObjectMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@Configuration
@EnableRedisHttpSession
public class CommonConfig {

    @Bean
    public RedisSerializer<Object> springSessionDefaultRedisSerializer(
            @Qualifier("redisSessionObjectMapper") RedisSessionObjectMapper redisSessionObjectMapper) {
        return new GenericJackson2JsonRedisSerializer(redisSessionObjectMapper);
    }

    @Bean
    @Primary
    public ObjectMapper defaultObjectMapper() {
        return new ObjectMapper();
    }

    @Bean
    @Qualifier("prettyPrintObjectMapper")
    public ObjectMapper prettyPrintObjectMapper() {
        return new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
    }
}
