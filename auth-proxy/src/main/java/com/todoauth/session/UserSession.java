package com.todoauth.session;

import com.todoauth.authentication.BasicProfile;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import java.io.Serializable;

@Component
@SessionScope
public class UserSession implements Serializable {
    private BasicProfile profile;
    private String accessToken;
    private String refreshToken;

    public BasicProfile getProfile() {
        return profile;
    }

    public void setProfile(BasicProfile profile) {
        this.profile = profile;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
