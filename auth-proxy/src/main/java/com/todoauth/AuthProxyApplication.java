package com.todoauth;

import com.todoauth.authentication.OAuthClientContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.*;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.token.AccessTokenRequest;
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;

import java.util.Map;

@SpringBootApplication
@EnableAutoConfiguration
@EnableOAuth2Client
@EnableZuulProxy
@Configuration
public class AuthProxyApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthProxyApplication.class, args);
	}

	@Bean
	@Primary
	@Scope(value = "session", proxyMode = ScopedProxyMode.INTERFACES)
	public OAuth2ClientContext oauth2ClientContext(AccessTokenRequest accessTokenRequest) {
		return new OAuthClientContext(accessTokenRequest);
	}

	@Bean
	@Primary
	@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
	protected DefaultAccessTokenRequest accessTokenRequest(@Value("#{request.parameterMap}")
															Map<String, String[]> parameters, @Value("#{request.getAttribute('currentUri')}")
															String currentUri) {
		DefaultAccessTokenRequest request = new DefaultAccessTokenRequest(parameters);
		request.setCurrentUri(currentUri);
		return request;
	}
}
