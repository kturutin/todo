package com.todoauth.filter.oauth2

import com.fasterxml.jackson.databind.ObjectMapper
import com.todoauth.authentication.BasicProfile
import com.todoauth.authentication.google.GoogleProfile
import com.todoauth.session.UserSession
import org.springframework.security.core.Authentication
import org.springframework.security.oauth2.client.OAuth2ClientContext
import org.springframework.security.oauth2.common.OAuth2AccessToken
import org.springframework.security.oauth2.provider.OAuth2Authentication
import spock.lang.Specification

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.servlet.http.HttpSession

class AbstractAuthSuccessHandlerTest extends Specification {
    def session = Mock(UserSession)
    def oauthContext = Mock(OAuth2ClientContext)
    def req = Mock(HttpServletRequest)
    def resp = Mock(HttpServletResponse)

    def "populate UserSession on successful authentication"() {
        given:
        def handler = new AbstractAuthSuccessHandler(session, oauthContext, new ObjectMapper()) {
            @Override
            BasicProfile mapAuthToProfile(OAuth2Authentication authResult) {
                new GoogleProfile(id: authResult.userAuthentication.details['id'])
            }
        }
        def auth = Mock(OAuth2Authentication)
        def userAuth = Mock(Authentication)
        def token = Mock(OAuth2AccessToken)
        def userDetails = [id: '123', givenName: 'givenName']
        def tokenValue = 'tokenValue'

        when:
        oauthContext.getAccessToken() >> token
        auth.getUserAuthentication() >> userAuth
        userAuth.getDetails() >> userDetails
        token.getValue() >> tokenValue
        req.getSession() >> Mock(HttpSession)

        and:
        handler.onAuthenticationSuccess(req, resp, auth)

        then:
        1 * session.setProfile({
            it.id == userDetails.id
            it.givenName = userDetails.givenName
        } as BasicProfile)

        and:
        1 * session.setAccessToken({
            it == tokenValue
        } as String)
    }

    def "throw exception if authentication type is not oauth2"() {
        given:
        def handler = new AbstractAuthSuccessHandler(session, oauthContext, new ObjectMapper()) {
            @Override
            BasicProfile mapAuthToProfile(OAuth2Authentication authResult) {
                new GoogleProfile(id: '123')
            }
        }

        when:
        handler.onAuthenticationSuccess(req, resp, Mock(Authentication))

        then:
        thrown(IllegalStateException)
    }
}
