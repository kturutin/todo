package com.todoauth.controller.proxy

import org.springframework.boot.autoconfigure.web.DefaultErrorAttributes
import spock.lang.Specification

import javax.servlet.http.HttpServletRequest

class ErrorControllerTest extends Specification {
    HttpServletRequest request
    DefaultErrorAttributes errorAttributes
    ErrorController errorController

    def setup() {
        request = Mock(HttpServletRequest)
        errorAttributes = Mock(DefaultErrorAttributes)
        errorController = new ErrorController(errorAttributes)
    }

    def "return error message from request attributes if there's one"() {
        given:
        errorController = new ErrorController(new DefaultErrorAttributes())
        def errorMsg = Mock(ErrorMessage)

        when:
        request.getAttribute(ErrorController.ERROR_ATTRIBUTES_REQUEST_ATTRIBUTE) >> errorMsg

        then:
        errorMsg == errorController.handleError(request)
    }

    def "shouldn't hide error details if it's not 5xx error"() {
        given:
        def errorAttributesData = [
                message: 'original message',
                error: 404,
                exception: new IllegalStateException()
        ]

        when:
        request.getAttribute("javax.servlet.error.status_code") >> 404
        errorAttributes.getErrorAttributes(_, false) >> errorAttributesData

        then:
        def errorMessage = errorController.handleError(request)

        errorMessage[ErrorMessage.Field.MESSAGE] == errorAttributesData.message
        errorMessage[ErrorMessage.Field.EXCEPTION] == errorAttributesData.exception
    }

    def "shouldn hide error details if it's 5xx error"() {
        given:
        def errorAttributesData = [
                message: 'original message',
                error: 500,
                exception: new IllegalStateException()
        ]

        when:
        request.getAttribute("javax.servlet.error.status_code") >> 500
        errorAttributes.getError(_) >> new Exception()
        errorAttributes.getErrorAttributes(_, false) >> errorAttributesData

        then:
        def errorMessage = errorController.handleError(request)

        errorMessage[ErrorMessage.Field.MESSAGE] != errorAttributesData.message
        errorMessage[ErrorMessage.Field.MESSAGE].toString().contains('Meditation')
        !errorMessage[ErrorMessage.Field.EXCEPTION]
    }
}
