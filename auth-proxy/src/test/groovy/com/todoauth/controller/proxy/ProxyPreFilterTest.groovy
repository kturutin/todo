package com.todoauth.controller.proxy

import com.netflix.zuul.context.RequestContext
import com.todoauth.authentication.google.GoogleProfile
import com.todoauth.session.UserSession
import com.todocommon.Roles
import com.todocommon.authentication.jwt.Claim
import com.todocommon.authentication.jwt.TokenParser
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import spock.lang.Specification

class ProxyPreFilterTest extends Specification {

    def "add JWT to the request headers"() {
        given:
        def session = Mock(UserSession)
        def profile = new GoogleProfile(
                id: '123',
                givenName: 'givenName',
                familyName: 'familyName',
                email: 'abc@abc.abc',
                name: 'givenName familyName'
        )
        def tokenName = 'tokenName'
        def tokenSecret = 'tokenSecret'
        def proxyPreFilter = new ProxyPreFilter(session, tokenName, tokenSecret)
        def zuulContext = Mock(RequestContext)
        def securityContext = Mock(SecurityContext)
        def authentification = Mock(Authentication)

        RequestContext.testSetCurrentContext(zuulContext)
        SecurityContextHolder.setContext(securityContext)

        when:
        authentification.getAuthorities() >> [new SimpleGrantedAuthority(Roles.ROLE_USER.name())]
        securityContext.getAuthentication() >> authentification
        session.getProfile() >> profile

        and:
        proxyPreFilter.run()

        then:
        1 * zuulContext.addZuulRequestHeader(tokenName, {
            def addedToken = TokenParser.parseToken(it, tokenSecret)
            addedToken[Claim.ID] == profile.id
            addedToken[Claim.GIVEN_NAME] == profile.givenName
            addedToken[Claim.FAMILY_NAME] == profile.familyName
        })
    }
}
