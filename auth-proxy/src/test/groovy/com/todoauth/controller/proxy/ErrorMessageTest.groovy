package com.todoauth.controller.proxy

import spock.lang.Specification

import static com.todoauth.controller.proxy.ErrorMessage.Field.*

class ErrorMessageTest extends Specification {

    def "parse attributes (no matter what case is used)"() {
        given:
        def data = [
                timestamp: 'timestamp value',
                mEssaGe: 'message value',
                ERROR: '400'
        ]

        expect:
        def errorMessage = new ErrorMessage(data)
        errorMessage.get(TIMESTAMP) == data.timestamp
        errorMessage.get(MESSAGE) == data.mEssaGe
        errorMessage.get(ERROR) == data.ERROR
    }

}
