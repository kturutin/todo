'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  BACKEND_URL: '"http://localhost:8070/api"',
  BACKEND_FACEBOOK_LOGIN_URL: '"http://localhost:8070/login/facebook"',
  BACKEND_GOOGLE_LOGIN_URL: '"http://localhost:8070/login/google"'
})

