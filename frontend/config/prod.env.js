'use strict'
module.exports = {
  NODE_ENV: '"production"',
  BACKEND_URL: '"http://localhost:8070/api"',
  BACKEND_FACEBOOK_LOGIN_URL: '"http://localhost:8070/login/facebook"',
  BACKEND_GOOGLE_LOGIN_URL: '"http://localhost:8070/login/google"'
}
