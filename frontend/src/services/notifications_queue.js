class NotificationsQueue {
  constructor () {
    this.queue = []
  }

  poll () {
    return this.queue.shift()
  }

  /**
   * message format:
   * {
          group: '',
          position: '',
          title: '',
          type: 'error|warn|success',
          text: ''
     }
   * @param message
   */
  push (message) {
    this.queue.push(message)
  }
}

export let notificationsQueue = new NotificationsQueue()
