import Axios from 'axios'
import store from '../vuex/store.js'
import routers from '../router/index.js'
import { notificationsQueue } from '../services/notifications_queue'

export default class LoginService {
  login () {
    if (!store.getters.isLoggedIn) {
      let request = { method: 'get', withCredentials: true }
      Axios(process.env.BACKEND_URL, request)
        .then(LoginService.handleSuccessful)
        .catch(LoginService.handleError)
    }
  }

  static handleSuccessful (response) {
    store.commit('setLoggedIn', true)
    store.commit('setUserDetails', response.data)
    notificationsQueue.push({
      title: 'Successfully connected',
      text: 'Successfully connected to the API server',
      type: 'success'
    })
  }

  static handleError (error) {
    store.commit('setLoggedIn', false)
    if (error.response === undefined || error.response.status === undefined) {
      notificationsQueue.push(
        {
          title: 'INTERNAL ERROR',
          text: 'API server is not available',
          type: 'error'
        }
      )
    } else if (error.response.status === 401) {
      routers.push('/login')
    }
  }
}
