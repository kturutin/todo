import { notificationsQueue } from '../services/notifications_queue.js'

export const DEFAULT_NOTIFICATIONS_GROUP = 'general-notifications'

class NotificationsDaemon {
  constructor () {
    this.queue = []
  }

  start (vueContext) {
    setInterval(() => {
      for (let i = 0; i < notificationsQueue.queue.length; i++) {
        let element = notificationsQueue.poll()
        element.group = Object.is(element.group, undefined) ? DEFAULT_NOTIFICATIONS_GROUP : element.group
        vueContext.$notify(element)
      }
    }, 1000)
  }
}

export let notificationsDaemon = new NotificationsDaemon()
