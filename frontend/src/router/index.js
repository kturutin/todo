import Vue from 'vue'
import Router from 'vue-router'
import Basic from '@/components/Root'
import Login from '@/components/Login'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      alias: '/_=_',
      name: 'Basic',
      component: Basic
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    }
  ]
})
