import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isLoggedIn: false,
    userDetails: undefined
  },
  getters: {
    isLoggedIn: function (state) {
      return state.isLoggedIn
    },
    getUserDetails: function (state) {
      return state.userDetails
    }
  },
  mutations: {
    setLoggedIn: function (state, isLoggedIn) {
      state.isLoggedIn = isLoggedIn
    },
    setUserDetails: function (state, userDetails) {
      state.userDetails = userDetails
    }
  }
})
