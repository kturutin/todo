import Vue from 'vue'
import App from './App'
import router from './router'
import Vuex from 'vuex'
import Notifications from 'vue-notification'
import store from './vuex/store.js'
import velocity from 'velocity-animate'
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)
Vue.use(Notifications, { velocity })
Vue.use(Vuex)
Vue.use(Notifications)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  store,
  components: { App }
})
