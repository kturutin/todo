CREATE TABLE public.user (
  id BIGSERIAL NOT NULL,
  version INTEGER NOT NULL,
  user_id TEXT NOT NULL,
  given_name TEXT,
  family_name TEXT,
  role TEXT NOT NULL,
  email TEXT,

  CONSTRAINT user_id_primary_key PRIMARY KEY (id),
  CONSTRAINT user_user_id_unique UNIQUE (user_id)
)
WITH (
  OIDS = FALSE
);