package com.todo.config;

import com.todocommon.authentication.AuthenticationFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

@Configuration
public class ConfigsProvider {

    @Bean
    public AuthenticationFilter getAuthenticationFilter(@Value("${authentication.tokenHeaderName}") String tokenHeaderName,
                                                        @Value("${authentication.tokenSecretKey}") String secret,
                                                        @Value("${authentication.protectUrlPattern}") String protectUrlPattern) {
        return new AuthenticationFilter(protectUrlPattern, tokenHeaderName, secret);
    }

    /**
     * Disable auto-registration
     */
    @Bean
    public FilterRegistrationBean authenticationFilterRegistration(AuthenticationFilter authenticationFilter) {
        FilterRegistrationBean reg = new FilterRegistrationBean(authenticationFilter);
        reg.setEnabled(false);
        return reg;
    }

    @Bean
    public AuthenticationProvider authenticationProvider() {
        return new AuthenticationProvider() {
            @Override
            public Authentication authenticate(Authentication authentication) throws AuthenticationException {
                return authentication;
            }

            @Override
            public boolean supports(Class<?> authentication) {
                return authentication.equals(UsernamePasswordAuthenticationToken.class);
            }
        };
    }
}
