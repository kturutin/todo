package com.todo.config;

import com.todocommon.authentication.AuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final AuthenticationFilter authFilter;
    private final String protectUrlPattern;
    private final AuthenticationProvider authenticationProvider;

    @Autowired
    public SecurityConfig(AuthenticationFilter authFilter,
                          AuthenticationProvider authProvider,
                          @Value("${authentication.protectUrlPattern}") String protectUrlPattern) {
        this.authFilter = authFilter;
        this.authenticationProvider = authProvider;
        this.protectUrlPattern = protectUrlPattern;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authenticationProvider(authenticationProvider);

        authFilter.setAuthenticationSuccessHandler((req, res, auth) -> {
            req.getRequestDispatcher(req.getRequestURI()).forward(req, res);
        });

        http.
                sessionManagement().
                sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.
                antMatcher(protectUrlPattern).
                anonymous().
                disable();

        http.
                addFilterBefore(authFilter, FilterSecurityInterceptor.class);

        super.configure(http);
    }
}
