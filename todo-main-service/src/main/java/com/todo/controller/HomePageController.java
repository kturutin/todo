package com.todo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@RestController
public class HomePageController {

    @RequestMapping("/**")
    public Object home(HttpServletRequest request, Principal principal) {
        return principal;
    }
}
