package com.todocommon.authentication;

import com.todocommon.authentication.jwt.Claim;
import com.todocommon.authentication.jwt.IllegalTokenException;
import com.todocommon.authentication.jwt.TokenParser;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.util.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This filter can be used in all the services behind 'auth-proxy' to set Authentication object to
 * the context from by-value JWT received from 'auth-proxy'
 */
public class AuthenticationFilter extends AbstractAuthenticationProcessingFilter {
    private static final String ROLES_SPLIT_SYMBOL = ",";
    private static final String CREDENTIALS = "";

    private final String tokenHeaderName;
    private final String tokenSecretKey;

    public AuthenticationFilter(String defaultFilterProcessesUrl,
                                   String tokenHeaderName,
                                   String tokenSecretKey) {
        super(defaultFilterProcessesUrl);
        setAuthenticationManager(authentication -> authentication);
        this.tokenHeaderName = tokenHeaderName;
        this.tokenSecretKey = tokenSecretKey;
    }

    public AuthenticationFilter(RequestMatcher requiresAuthenticationRequestMatcher,
                                   String tokenHeaderName,
                                   String tokenSecretKey) {
        super(requiresAuthenticationRequestMatcher);
        setAuthenticationManager(authentication -> authentication);
        this.tokenHeaderName = tokenHeaderName;
        this.tokenSecretKey = tokenSecretKey;
    }

    @Override
    protected boolean requiresAuthentication(HttpServletRequest request, HttpServletResponse response) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        return authentication == null && super.requiresAuthentication(request, response);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException, IOException, ServletException {
        Optional<String> token = getTokenValue(request);
        if (token.isPresent()) {
            return attemptAuthenticationByToken(token.get());
        } else {
            throw new AuthenticationExceptionIllegalToken("No token available in request");
        }
    }

    private Authentication attemptAuthenticationByToken(String token) {
        try {
            return createAuthenticationByClaims(getParsedToken(token));
        } catch (IllegalTokenException ex) {
            throw new AuthenticationExceptionIllegalToken("Illegal token", ex);
        }
    }

    private Authentication createAuthenticationByClaims(Map<Claim, String> claims) {
        String role = claims.get(Claim.ROLE);
        UsernamePasswordAuthenticationToken authentication =
                new UsernamePasswordAuthenticationToken(claims.get(Claim.ID), CREDENTIALS, getAuthorities(role));
        authentication.setDetails(claims);
        return authentication;
    }

    private Collection<? extends GrantedAuthority> getAuthorities(String roles) {
        if (StringUtils.isEmpty(roles))
            throw new IllegalTokenException("Couldn't get authorities from claims, no roles available");

        return Stream.of(roles.split(ROLES_SPLIT_SYMBOL)).
                map(role -> new SimpleGrantedAuthority(role.toUpperCase())).
                collect(Collectors.toList());
    }

    private Map<Claim, String> getParsedToken(String token) {
        return TokenParser.parseToken(token, tokenSecretKey);
    }

    private Optional<String> getTokenValue(HttpServletRequest request) {
        return Optional.ofNullable(request.getHeader(tokenHeaderName));
    }

    public static class AuthenticationExceptionIllegalToken extends AuthenticationException {
        public AuthenticationExceptionIllegalToken(String msg) {
            super(msg);
        }

        public AuthenticationExceptionIllegalToken(String msg, Throwable t) {
            super(msg, t);
        }
    }
}
