package com.todocommon.authentication.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TokenParser {

    public static Map<Claim, String> parseToken(String token, String secret) throws IllegalTokenException {
        Object body = getBody(token, secret);
        if (body instanceof Claims) {
            Map<Claim, String> result = bodyToClaims((Claims) body);
            if (result.size() != Claim.values().length) {
                throw new IllegalTokenException("Invalid token, not all the necessary claims are provided");
            }
            return result;
        } else {
            throw new IllegalTokenException("Unknown JWT claims format");
        }
    }

    private static Map<Claim, String> bodyToClaims(Claims claims) {
        Map<Claim, String> result = new HashMap<>();

        Stream.of(Claim.values()).forEach(claim -> {
            Object value = claims.get(claim.getJwtName());
            if (value != null) {
                result.put(claim, String.valueOf(value));
            }
        });

        return result;
    }

    private static Object getBody(String token, String secret) {
        try {
            return Jwts.parser().setSigningKey(secret.getBytes()).parse(token).getBody();
        } catch (Exception ex) {
            throw new IllegalTokenException("Couldn't parse token", ex);
        }
    }
}
