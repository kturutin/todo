package com.todocommon.authentication.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Map;

public class TokenCreator {

    public static String createToken(Map<Claim, String> claims, String secret) {
        return Jwts.builder().
                setClaims(createClaims(claims)).
                signWith(SignatureAlgorithm.HS256, secret.getBytes()).
                compact();
    }

    private static Claims createClaims(Map<Claim, String> claims) {
        Claims result = Jwts.claims();
        claims.forEach((k, v) -> result.put(k.getJwtName(), v));
        return result;
    }
}
