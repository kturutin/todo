package com.todocommon.authentication.jwt;

public enum Claim {
    ID("jti"),
    SUBJECT("sub"),
    GIVEN_NAME,
    FAMILY_NAME,
    ROLE,
    EMAIL;

    private final String jwtName;

    Claim(String jwtName) {
        this.jwtName = jwtName;
    }

    Claim() {
        this.jwtName = name().toLowerCase();
    }

    public String getJwtName() {
        return jwtName.toLowerCase();
    }
}
