package com.todocommon.authentication.jwt;

public class IllegalTokenException extends IllegalStateException {

    public IllegalTokenException(String msg) {
        super(msg);
    }

    public IllegalTokenException(String message, Throwable cause) {
        super(message, cause);
    }
}
