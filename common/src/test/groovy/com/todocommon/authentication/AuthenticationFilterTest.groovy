package com.todocommon.authentication

import com.todocommon.authentication.jwt.Claim
import spock.lang.Specification

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import static com.todocommon.authentication.jwt.TokenCreator.createToken

class AuthenticationFilterTest extends Specification {

    def secret = '123'
    def tokenHeaderName = 'token'
    def claims = [
            (Claim.ID)         : '123',
            (Claim.GIVEN_NAME) : 'ABC',
            (Claim.FAMILY_NAME): 'ABC',
            (Claim.EMAIL)      : 'abc@abc.com',
            (Claim.ROLE)       : 'ROLE_USER',
            (Claim.SUBJECT)    : 'subject',
    ]
    def resp = Mock(HttpServletResponse)
    def req = Mock(HttpServletRequest)
    def filter = new AuthenticationFilter('/**', tokenHeaderName, secret)

    def "create authentication object from provided JWT with correct claims as auth details"() {
        given:
        req.getHeader(tokenHeaderName) >> createToken(claims, secret)

        when:
        def auth = filter.attemptAuthentication(req, resp)

        then:
        auth.details == claims
        auth.authorities.first().toString() == claims[Claim.ROLE]
    }

    def "throws exception if token secret is invalid"() {
        given:
        req.getHeader(tokenHeaderName) >> createToken(claims, '345')

        when:
        filter.attemptAuthentication(req, resp)

        then:
        thrown(AuthenticationFilter.AuthenticationExceptionIllegalToken)
    }

    def "throws exception if token is invalid"() {
        given:
        req.getHeader(tokenHeaderName) >> '123'

        when:
        filter.attemptAuthentication(req, resp)

        then:
        thrown(AuthenticationFilter.AuthenticationExceptionIllegalToken)
    }
}
