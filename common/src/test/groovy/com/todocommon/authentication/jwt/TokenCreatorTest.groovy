package com.todocommon.authentication.jwt

import spock.lang.Specification

class TokenCreatorTest extends Specification implements TokenTest {

    def "create correct jwt"() {
        given:
        def secret = '123'

        when:
        def token = TokenCreator.createToken(claims, secret)

        then:
        claims == TokenParser.parseToken(token, secret)
    }
}
