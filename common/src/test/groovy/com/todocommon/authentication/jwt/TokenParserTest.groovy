package com.todocommon.authentication.jwt

import spock.lang.Specification

import static com.todocommon.authentication.jwt.TokenCreator.createToken
import static com.todocommon.authentication.jwt.TokenParser.parseToken

class TokenParserTest extends Specification implements TokenTest {

    def "parse successfully if claims and secret are valid"() {
        given:
        def secret = '123'
        def jwt = createToken(claims, secret)

        when:
        def parsedJwt = parseToken(jwt, secret)

        then:
        parsedJwt == claims
    }

    def "throws IllegalTokenException if not all claims provided"() {
        given:
        claims.remove(Claim.SUBJECT)
        def secret = '123'
        def jwt = createToken(claims, secret)

        when:
        parseToken(jwt, secret)

        then:
        thrown(IllegalTokenException)
    }

    def "throws IllegalTokenException if invalid secret is provided"() {
        given:
        def secret = '123'
        def jwt = createToken(claims, secret)

        when:
        parseToken(jwt, '456')

        then:
        thrown(IllegalTokenException)
    }

}
