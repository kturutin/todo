package com.todocommon.authentication.jwt

trait TokenTest {

    Map<Claim, String> claims = [
            (Claim.ID) : '123',
            (Claim.GIVEN_NAME) : 'ABC',
            (Claim.FAMILY_NAME) : 'ABC',
            (Claim.EMAIL) : 'abc@abc.com',
            (Claim.ROLE) : 'ROLE_USER',
            (Claim.SUBJECT) : 'subject',
    ]
}
