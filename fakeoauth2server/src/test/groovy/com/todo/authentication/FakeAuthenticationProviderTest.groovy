package com.todo.authentication

import com.todocommon.Roles
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import spock.lang.Specification

class FakeAuthenticationProviderTest extends Specification {

    def "provide authentication with default role if both principal & credentials are valid"() {
        given:
        def userName = 'testUser'
        def password = 'password'
        def provider = new FakeAuthenticationProvider([(userName): password])
        def auth = Mock(Authentication)

        and:
        auth.principal >> userName
        auth.credentials >> password

        when:
        def result = provider.authenticate(auth)

        then:
        result in UsernamePasswordAuthenticationToken
        result.credentials == password
        result.principal == userName
        result.authorities.first().authority == Roles.ROLE_USER.name()
    }

    def "return null if principal or credentials is invalid"() {
        given:
        def userName = 'testUser'
        def password = 'password'
        def provider = new FakeAuthenticationProvider([(userName): password])
        def auth = Mock(Authentication)

        and:
        auth.principal >> userName
        auth.credentials >> 'invalid_password'

        when:
        def result = provider.authenticate(auth)

        then:
        !result
    }
}
