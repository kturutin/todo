package com.todo

import org.springframework.http.HttpStatus
import spock.lang.Specification

import java.security.Principal

class UsersControllerTest extends Specification {

    def "return user details by principal if exist"() {
        given:
        def userDetails = [id: 123]
        def userName = 'user'
        def controller = new UsersController([(userName): userDetails])
        def principal = Mock(Principal)
        principal.name >> userName

        when:
        def result = controller.getUserDetails(principal)

        then:
        result.body == userDetails
        result.statusCode == HttpStatus.OK
    }

    def "return 404 if no details found"() {
        given:
        def userDetails = [id: 123]
        def userName = 'user'
        def controller = new UsersController([(userName): userDetails])
        def principal = Mock(Principal)
        principal.name >> 'anotherUser'

        when:
        def result = controller.getUserDetails(principal)

        then:
        result.statusCode == HttpStatus.NOT_FOUND
    }
}
