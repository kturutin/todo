package com.todo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@SpringBootApplication
@EnableConfigurationProperties
public class FakeOAuth2ServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(FakeOAuth2ServerApplication.class, args);
    }
}
