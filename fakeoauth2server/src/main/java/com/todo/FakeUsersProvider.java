package com.todo;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class FakeUsersProvider {

    @Bean
    @ConfigurationProperties(prefix = "userProfiles")
    @Qualifier("userProfiles")
    public HashMap<String, Object> userProfiles() {
        return new HashMap<>();
    }

    @Bean
    @ConfigurationProperties(prefix = "credentials")
    @Qualifier("credentials")
    public HashMap<String, String> userCredentials() {
        return new HashMap<>();
    }
}
