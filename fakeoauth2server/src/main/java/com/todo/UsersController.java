package com.todo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.HashMap;

@RestController
public class UsersController {
    private final HashMap<String, Object> userProfiles;

    @Autowired
    public UsersController(@Qualifier("userProfiles") HashMap<String, Object> userProfiles) {
        this.userProfiles = userProfiles;
    }

    @RequestMapping("/user")
    public ResponseEntity<Object> getUserDetails(Principal principal) {
        Object profile = userProfiles.get(principal.getName());

        return profile == null ?
                new ResponseEntity<>(HttpStatus.NOT_FOUND) :
                new ResponseEntity<>(profile, HttpStatus.OK);
    }
}
