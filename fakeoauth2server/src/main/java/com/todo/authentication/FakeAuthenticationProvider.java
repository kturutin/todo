package com.todo.authentication;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

import static com.todocommon.Roles.ROLE_USER;

@Component
public class FakeAuthenticationProvider implements AuthenticationProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(FakeAuthenticationProvider.class);

    private final HashMap<String, String> credentials;

    @Autowired
    public FakeAuthenticationProvider(@Qualifier("credentials") HashMap<String, String> credentials) {
        this.credentials = credentials;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Object principal = authentication.getPrincipal();
        Object password = authentication.getCredentials();

        if (principal instanceof String && password instanceof String) {

            if (StringUtils.equals(credentials.get(principal), (String) password)) {
                return new UsernamePasswordAuthenticationToken(principal, password, getDefaultAuthorities());
            }

        } else {
            LOGGER.warn("Unknown principal/credentials format, principal: {}, credentials: {}", principal, password);
        }

        return null;
    }

    private Collection<? extends GrantedAuthority> getDefaultAuthorities() {
        GrantedAuthority authority = new SimpleGrantedAuthority(ROLE_USER.name());
        return Collections.singleton(authority);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return UsernamePasswordAuthenticationToken.class.equals(authentication);
    }
}
