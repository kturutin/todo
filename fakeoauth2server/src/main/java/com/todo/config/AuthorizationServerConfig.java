package com.todo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.stereotype.Component;

@EnableAuthorizationServer
@Component
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {
    @Value("${auth.server.allowedClientId}")
    private String allowedClientId;

    @Value("${auth.server.allowedClientSecret}")
    private String allowedClientSecret;

    @Value("${auth.server.scope}")
    private String[] scopes;

    @Value("${auth.server.autoApproveScopes}")
    private String[] autoApproveScopes;

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.
                inMemory().
                withClient(allowedClientId).
                secret(allowedClientSecret).
                scopes(scopes).
                autoApprove(autoApproveScopes).
                authorizedGrantTypes("authorization_code", "refresh_token");
    }
}
