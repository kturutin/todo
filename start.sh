#!/usr/bin/env bash

./stop.sh

profile=$1

docker-compose build
docker-compose up -d "${profile:-default}"