package com.todo.context

import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.util.MultiValueMap

/**
 * POJO to hold http request/response state.
 * Spring's HttpEntity is inconvenient for tests due to it's immutability.
 */
class HttpInteraction {
    MultiValueMap<String, String> headers
    Object body
    URI uri
    HttpMethod method
    HttpStatus status
}
