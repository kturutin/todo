package com.todo.context

import com.todo.annotation.ScenarioScope
import org.springframework.stereotype.Component

@Component
@ScenarioScope
class ScenarioContext {

    Map<String, HttpInteraction> requests = new HashMap<>()
    Map<String, HttpInteraction> responses = new HashMap<>()

    def addReq(String name, HttpInteraction entity) {
        requests.put(name, entity)
    }

    def addResp(String name, HttpInteraction entity) {
        responses.put(name, entity)
    }

    HttpInteraction getReq(String name) {
        requests.get(name)
    }

    HttpInteraction getReqRequired(String name) {
        def result = getReq(name)

        if (!result) {
            throw new IllegalStateException('No request available among prepared, consider preparing request before using it')
        }

        result
    }

    HttpInteraction getResp(String name) {
        responses.get(name)
    }

}
