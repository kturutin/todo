package com.todo.docker

import com.todo.utils.YamlProperties
import groovy.json.JsonSlurper
import groovy.transform.Memoized
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import javax.annotation.PostConstruct

import static com.todo.utils.YamlProperties.ConfigLocation.DOCKER_COMPOSE

@Component
class ComposeClient {
    static def LOGGER = LoggerFactory.getLogger(ComposeClient.class)

    static def DOCKER = 'docker'
    static def CONTAINER_STATE = "${DOCKER} inspect "

    static def COMPOSE = 'docker-compose'
    static def COMPOSE_CONTAINERS_LIST = "${COMPOSE} ps -q"

    @Autowired
    YamlProperties properties

    @PostConstruct
    def init() {
        addShutdownHook {
            LOGGER.info("Shutdown signal, going to stop all containers")
            stopContainers()
        }
    }

    def build() {
        execCmd("${COMPOSE} build")
    }

    def stop() {
        execCmd("${COMPOSE} stop")
    }

    def up() {
        if (containerIds) {
            LOGGER.warn('Found running containers, going to stop them before')
            stopContainers()
        }
        LOGGER.info('Starting containers, it might take some time.')
        execCmd("${COMPOSE} up -d test")
        waitForHealthyState()
    }

    def waitForHealthyState(int delay = 500, int attempts = 50) {
        LOGGER.info("Waiting the containers: ${getAllDependencies('test')} to start")

        def start = System.currentTimeSeconds()
        def getTimePassed = {"${System.currentTimeSeconds() - start} sec"}

        (1..attempts).any {
            if (areAllServicesHealthy()) {
                LOGGER.info("Containers are healthy, it took ${getTimePassed()}")
                return true
            } else {
                if (it == attempts) {
                    LOGGER.error("Unhealthy services: \n${unhealthyServiceNames.join('\n')}")
                    throw new IllegalStateException("Failed to start containers, unhealthy containers found")
                }
                LOGGER.info("Waiting for ${unhealthyServiceNames}, passed ${getTimePassed()}")
                sleep(delay)
            }

            false
        }
    }

    def areAllServicesHealthy() {
        !unhealthyServiceNames
    }

    def getHealthyContainerNames() {
        healthyContainerStates.collect { it['Name'] as String }
    }

    def getUnhealthyServiceNames() {
        serviceNames.findAll { !isHealthyServiceAvailable(it) }
    }

    def getHealthyContainerStates() {
        healthyContainerIds.collect { getContainerInfo(it) }
    }

    def getHealthyContainerIds() {
        containerIds.findAll { isContainerHealthy(it) }
    }

    def isHealthyServiceAvailable(String serviceName) {
        healthyContainerNames.any { it.contains(serviceName) }
    }

    def isContainerHealthy(String containerId) {
        def info = getContainerInfo(containerId)
        def isTempContainer = info?.HostConfig?.RestartPolicy?.Name == 'no'

        return isTempContainer || isRunningContainerHealthy(info.State)
    }

    def isRunningContainerHealthy(def state) {
        state?.Status == 'running' && (state?.Health?.Status == 'healthy' || !state?.Health?.Status)
    }

    @Memoized
    def getAllDependencies(String serviceName) {
        Set collector = new HashSet()
        collectDependencies(serviceName, collector)
        collector
    }

    def collectDependencies(String serviceName, Set collector) {
        def services = properties.get('services', DOCKER_COMPOSE) as Map
        def dependencies = services[serviceName]['depends_on'] as List

        if (dependencies) {
            collector.addAll(dependencies)
            dependencies.forEach( { collectDependencies(it as String, collector)})
        }
    }

    def stopContainers() {
        execCmd("${COMPOSE} down")
    }

    def getContainerIds() {
        execCmd(COMPOSE_CONTAINERS_LIST).output.readLines()
    }

    @Memoized
    Collection<String> getServiceNames() {
        getAllDependencies('test')
    }

    def getContainerInfo(String containerId) {
        def parser = JsonSlurper.newInstance()
        def cmd = CONTAINER_STATE + containerId
        (parser.parseText(execCmd(cmd).output as String) as List).first()
    }

    OperationResult execCmd(String cmd) {
        def proc = cmd.execute()
        def output = new StringBuilder()
        def errOutput = new StringBuilder()

        proc.waitForProcessOutput(output, errOutput)

        def isSuccessful = proc.exitValue() == 0

        if (!isSuccessful)
            handleCmdFail(cmd, errOutput)

        new OperationResult(
                output: output,
                errOutput: errOutput,
                exitCode: proc.exitValue()
        )
    }

    def handleCmdFail(def cmd, def errOutput) {
        throw new CmdExecutionException(cmd, errOutput)
    }

    class CmdExecutionException extends RuntimeException {
        CmdExecutionException(def cmd, def errOutput) {
            super("Couldn't execute command: ${cmd}, error output: ${errOutput}")
        }
    }
}
