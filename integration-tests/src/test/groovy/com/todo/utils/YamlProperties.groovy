package com.todo.utils

import groovy.transform.Memoized
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.ResourceLoader
import org.springframework.stereotype.Component
import org.yaml.snakeyaml.Yaml

/**
 * This class helps reading yml configs.
 * Tree structure of the final object is required, so Spring's Environment can't be used (due to flat representation of configs)
 */
@Component
class YamlProperties {

    @Autowired
    ResourceLoader resourceLoader

    enum ConfigLocation {
        DOCKER_COMPOSE('file:./../docker-compose.yml'),
        APPLICATION('classpath:application.yml')

        final String location

        ConfigLocation(def location) {
            this.location = location
        }
    }

    @Memoized
    def get(String property, ConfigLocation config = ConfigLocation.APPLICATION) {
        def resource = resourceLoader.getResource(config.location)

        if (!resource.exists()) {
            throw new IllegalStateException("Couldn't find config: ${config.location}")
        }

        getConfig(resource.file)[property]
    }

    @Memoized
    private def getConfig(File file) {
        try {
            new Yaml().load(file.newInputStream())
        } catch (Exception ex) {
            throw new IllegalStateException("Couldn't parse yaml by location: ${file.absolutePath}", ex)
        }
    }

}
