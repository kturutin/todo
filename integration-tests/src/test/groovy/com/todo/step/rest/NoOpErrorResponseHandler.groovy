package com.todo.step.rest

import org.springframework.http.client.ClientHttpResponse
import org.springframework.stereotype.Component
import org.springframework.web.client.DefaultResponseErrorHandler

@Component
class NoOpErrorResponseHandler extends DefaultResponseErrorHandler {

    @Override
    void handleError(ClientHttpResponse response) throws IOException {
    }
}
