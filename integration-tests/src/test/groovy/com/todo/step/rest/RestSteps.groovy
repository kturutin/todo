package com.todo.step.rest

import com.todo.annotation.Steps
import com.todo.context.HttpInteraction
import com.todo.context.ScenarioContext
import com.todo.utils.YamlProperties
import cucumber.api.DataTable
import cucumber.api.java.en.When
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.RequestEntity
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder

@Steps
class RestSteps {
    @Autowired
    ScenarioContext context

    @Autowired
    YamlProperties properties

    @Autowired
    RestTemplate rest

    @When('^I prepare (GET|POST|PUT|DELETE) request \"(.*)\" on uri \"(.*)\" of service \"(.*)\"$')
    def prepareRequest(String httpMethod, String reqName, String path, String serviceName) {
        def services = properties.get('services') as Map<String, Object>
        def service = services.get(serviceName) as Map<String, String>

        if (!service) {
            throw new RuntimeException("Invalid service '${serviceName}' provided in feature, should be among ${services.keySet()}")
        }

        def interaction = new HttpInteraction(
                uri: buildUri(service, path),
                method: HttpMethod.resolve(httpMethod))

        context.addReq(reqName, interaction)
    }

    @When('^request \"(.*)\" has body$')
    def setRequestBody(String reqName, Object body) {
        context.getReqRequired(reqName).body = body
    }

    @When('^request \"(.*)\" has headers')
    def setRequestHeaders(String reqName, DataTable headersTable) {
        context.getReqRequired(reqName).headers = dataTableToHeaders(headersTable)
    }

    @When('^I send request \"(.*)\"')
    def sendPreparedRequest(String reqName) {
        def interaction = context.getReqRequired(reqName)

        def httpEntity = new RequestEntity(
                interaction.body,
                interaction.headers,
                interaction.method,
                interaction.uri)

        def response = rest.exchange(httpEntity, Object.class)

        context.addResp(reqName, new HttpInteraction(
                body: response.body,
                headers: response.headers,
                status: response.statusCode)
        )
    }

    private HttpHeaders dataTableToHeaders(DataTable headers) {
        def map = new HttpHeaders()
        headers.raw().each { it ->
            map.add(it[0], it[1])
        }
        map
    }

    private URI buildUri(Map<String, String> service, String path) {
        UriComponentsBuilder.newInstance().
                scheme(service['scheme']).
                host(service['host']).
                port(service['port']).
                path(path).
                build().
                toUri()
    }
}