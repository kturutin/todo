package com.todo.step

import com.todo.annotation.Steps
import com.todo.docker.ComposeClient
import cucumber.api.Scenario
import cucumber.api.java.After
import cucumber.api.java.Before
import org.springframework.beans.factory.annotation.Autowired

@Steps
class EnvironmentSteps {
    static def RESTART_REQUIRED_TAG = '@RestartRequired'

    @Autowired
    ComposeClient composeClient

    @Before()
    def setUp(Scenario scenario) {
        if (!composeClient.areAllServicesHealthy() || isRestartRequired(scenario)) {
            composeClient.build()
            composeClient.up()
        }
    }

    @After()
    def tearDown(Scenario scenario) {
        if (isRestartRequired(scenario)) {
            composeClient.stop()
        }
    }

    def isRestartRequired(Scenario scenario) {
        RESTART_REQUIRED_TAG in scenario.sourceTagNames
    }
}
