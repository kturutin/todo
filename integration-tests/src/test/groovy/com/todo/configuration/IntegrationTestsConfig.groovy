package com.todo.configuration

import com.todo.step.rest.NoOpErrorResponseHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestTemplate

@Configuration
@ComponentScan(basePackages = 'com.todo')
class IntegrationTestsConfig {

    @Bean
    RestTemplate restTemplate(NoOpErrorResponseHandler responseHandler) {
        return new RestTemplate(errorHandler: responseHandler)
    }
}
