package com.todo

import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber
import org.junit.runner.RunWith

@RunWith(Cucumber)
@CucumberOptions(features = 'src/test/resources')
class CucumberRunner {
}
