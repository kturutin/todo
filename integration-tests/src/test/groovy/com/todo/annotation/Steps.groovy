package com.todo.annotation

import com.todo.configuration.IntegrationTestsConfig
import groovy.transform.AnnotationCollector
import org.springframework.boot.test.context.SpringBootContextLoader
import org.springframework.test.context.ContextConfiguration

@AnnotationCollector
@ContextConfiguration(loader = SpringBootContextLoader, classes = IntegrationTestsConfig)
@interface Steps {}