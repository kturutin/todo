package com.todo.annotation

import groovy.transform.AnnotationCollector
import org.springframework.context.annotation.Scope

@AnnotationCollector
@Scope('cucumber-glue')
@interface ScenarioScope {
}